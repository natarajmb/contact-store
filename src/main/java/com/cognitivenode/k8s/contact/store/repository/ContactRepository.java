package com.cognitivenode.k8s.contact.store.repository;

import com.cognitivenode.k8s.contact.store.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;


/**
 * Repository for {@link Contact} database operations
 */
@RepositoryRestResource(collectionResourceRel = "contacts", path = "contacts")
public interface ContactRepository extends JpaRepository<Contact, UUID> {

    Contact getContactById(UUID id);

}
